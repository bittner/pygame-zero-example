Pygame Zero Example
===================

A simply example built from the [Pygame Zero tutorial](
https://pygame-zero.readthedocs.io/en/stable/introduction.html).

Usage
-----

1. Install dependencies using [Poetry](
https://python-poetry.org/docs/basic-usage/), e.g.
```console
poetry install
```

2. Run the game with the Pygame Zero CLI:
```console
pgzrun game.py
```

3. Try clicking on the pink alien and see what happens.
